package me.kodysimpson.staffhomes;

import me.kodysimpson.staffhomes.commands.StaffHomeCommand;
import org.bukkit.plugin.java.JavaPlugin;

public final class StaffHomes extends JavaPlugin {

    @Override
    public void onEnable() {
        System.out.println("StaffHomes plugin ONLINE");

        getConfig().options().copyDefaults();
        saveDefaultConfig();

        getCommand("staffhome").setExecutor(new StaffHomeCommand(this));
    }

    @Override
    public void onDisable() {
        System.out.println("StaffHomes plugin OFFLINE");
    }
}
